#!/bin/bash

TOOLCHAIN_QRB5165="/opt/cross_toolchain/aarch64-gnu-7.toolchain.cmake"

cd libfc-sensor-api
mkdir -p build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_QRB5165} ../
make -j$(nproc)
cd ../../

cd src
mkdir -p build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_QRB5165} ../
make -j$(nproc)
cd ../../