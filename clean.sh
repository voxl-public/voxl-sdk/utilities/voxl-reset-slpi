#!/bin/bash
#
# Modal AI Inc. 2024
# author: eric.katzfey@modalai.com


sudo rm -rf libfc-sensor-api/build/
sudo rm -rf src/build/
sudo rm -rf pkg/control.tar.gz
sudo rm -rf pkg/data/
sudo rm -rf pkg/data.tar.gz
sudo rm -rf pkg/DEB/
sudo rm -rf *.deb
sudo rm -rf .bash_history
